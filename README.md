### Default settings for TileOS Sway edition

Any issues with the default configuration of TileOS Sway should be reported to [this](https://gitlab.com/tile-os/tileos-settings-sway/-/issues) repository. Feel free to suggest your improvements and customizations!

<img src=sway.png>

Default theme: [Catppuccin Macchiato](https://github.com/catppuccin/catppuccin) (GTK, Kvantum, i3/Sway, Waybar and cursor). TileOS-specific artwork can be found [here](https://gitlab.com/tile-os/tileos-artwork).

Applications used:
* [Alacritty](https://github.com/alacritty/alacritty) - Terminal emulator;
* [Azote](https://github.com/nwg-piotr/azote) - Wallpaper manager;
* [Btop++](https://github.com/aristocratos/btop) - System load monitor
* [Calcurse](https://github.com/lfos/calcurse) - Calendar;
* [Cliphist](https://github.com/sentriz/cliphist) - Clipboard manager;
* [Dunst](https://github.com/dunst-project/dunst) - Notification system;
* dconf - GTK/GNOME specific settings manager (used for GTK applications);
* Grimshot - screenshot script;
* [Kvantum](https://github.com/tsujan/Kvantum) - Theme manager and engine for Qt-based applications;
* [Light](https://github.com/perkele1989/light) - Brightness control;
* [nwg-bar](https://github.com/nwg-piotr/nwg-bar) - Session management (shutdown, reboot, suspend, etc);
* [nwg-drawer](https://github.com/nwg-piotr/nwg-drawer) - Fullscreen application menu;
* [nwg-look](https://github.com/nwg-piotr/nwg-look) - Look-n-Feel configurator for GTK;
* [nwg-wrapper](https://github.com/nwg-piotr/nwg-wrapper) - Used for desktop overlay with a list of default keybindings;
* [Pavucontrol](https://github.com/pulseaudio/pavucontrol) - Pulseaudio/Pipewire volume control;
* [Playerctl](https://github.com/altdesktop/playerctl) - utility for control MPRIS-based players;
* [PCmanFM-Qt](https://github.com/lxqt/pcmanfm-qt) - File manager
* [Pulsemixer](https://github.com/GeorgeFilipkin/pulsemixer) - console-based volume control for Pulseaudio/Pipewire;
* [Pluma](https://github.com/mate-desktop/pluma) - GTK-based text editor;
* [Rofi](https://github.com/lbonn/rofi) - Application launcher with Wayland support (fork);
* [qt5ct](https://sourceforge.net/projects/qt5ct/) - Styling for Qt5-based applications;
* [qt6ct](https://github.com/trialuser02/qt6ct) - Styling for Qt6-based applications;
* [swappy](https://github.com/jtheoof/swappy) - Screenshot manager;
* [sway-systemd](https://github.com/alebastr/sway-systemd) - Systemd integration for Sway;
* [swayr](https://sr.ht/~tsdh/swayr/) - Advanced window switcher for Sway;
* [swaybg](https://github.com/swaywm/swaybg) - Wallpaper daemon;
* [swayidle](https://github.com/swaywm/swayidle) - Idle daemon/watcher;
* [swaylock](https://github.com/swaywm/swaylock) - Lockscreen daemon;
* [Waybar](https://github.com/Alexays/Waybar) - Topbar (panel) with various widgets;
* [wf-recorder](https://github.com/ammen99/wf-recorder) - Screen recorder;
* [wl-clipboard](https://github.com/bugaevc/wl-clipboard) - Wayland clipboard daemon;
* [wlsunset](https://sr.ht/~kennylevinsen/wlsunset/) - utility for control color temperature, also known as "Nigth Color Mode".