#!/bin/bash

export LC_ALL=C

TITLE="Select the output for the screencast"
OUT_NAME="HEADLESS-1"
FONT_SIZE="12"
ROSEWATER="#f4dbd6"

FONT="Noto Sans ${FONT_SIZE}"
FONT_COLOR="${ROSEWATER}"

I_SIZE="$(awk "BEGIN {print (${FONT_SIZE}*1.08)}")"
OUT_SIZE="$(awk "BEGIN {print (${FONT_SIZE}*0.94)}")"
TIP_SHIFT="$(awk "BEGIN {print (1+(${I_SIZE}-${FONT_SIZE})/2)}")"
TIP="\
<span color='${FONT_COLOR}'>\
<span size='${I_SIZE}pt'></span> \
<span baseline_shift='${TIP_SHIFT}pt'>use \
<span size='${OUT_SIZE}pt'>${OUT_NAME}</span> \
to cast only chosen windows\
</span>\
</span>\
"

swaymsg -t get_outputs \
  | jq -r '.[] | .name' \
  | rofi \
    -dmenu -i -l 4 \
    -p "${TITLE}" \
    -window-title "${TITLE}" \
    -mesg "${TIP}" \
    -font "${FONT}"
